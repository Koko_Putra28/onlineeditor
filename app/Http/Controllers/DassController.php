<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DassController extends Controller
{
    // dassboard
    public function dassboard_php()
    {
        return view('dassboard/dassboard-php');
    }

    public function dassboard_css()
    {
        return view('dassboard/dassboard-css');
    }

    public function dassboard_sql()
    {
        return view('dassboard/dassboard-sql');
    }

    public function dassboard_html()
    {
        return view('dassboard/dassboard-html');
    }

    public function dassboard_js()
    {
        return view('dassboard/dassboard-js');
    }

    public function dassboard_python()
    {
        return view('dassboard/dassboard-python');
    }
}
