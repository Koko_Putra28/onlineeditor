<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Php;

class admin_php extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_php = Php::all();

        return view('admin/admin_php/admin-php', ['php'=>$data_php]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/admin_php/admin-phpCreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data_php = new Php;

        $data_php->judul = $request->judul;
        $data_php->isi = $request->isi;
        $data_php->save();

        return redirect('/admin-php');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data_php = Php::where('id', $id)->get();

        return view('admin/admin_php/admin-phpEdit', ['edit_php' => $data_php]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update_php = Php::find($id);
        $update_php->judul = $request->judul;
        $update_php->isi = $request->isi;
        $update_php->update();

        return redirect('/admin-php');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hapus = Php::find($id);
        $hapus->delete();

        return redirect('/admin-php');
    }
}
