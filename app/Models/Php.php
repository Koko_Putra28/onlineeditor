<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Php extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'php';
    public $timestamps = false;
    protected $fillable = [
        'id', 'judul', 'isi', 'image', 'created_date'];
}
