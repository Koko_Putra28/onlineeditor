<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dasboard</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <link href="/css/style.css" rel="stylesheet">
    
</head>
<body>
    
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">
        <a class="navbar-brand me-5" href="#">OnlineEditor</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse ms-5" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Admin
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <table class="table">
                            <tr>
                                <th>
                                    <td class="px-0 py-0"><a class="dropdown-item" href="/admin-php">Admin-PHP</a></td>
                                    <td class="px-0 py-0"><a class="dropdown-item" href="/admin-html">Admin-HTML</a></td>
                                <th>
                            </tr>
                            <tr>
                                <th>
                                    <td class="px-0 py-0"><a class="dropdown-item" href="/admin-css">Admin-CSS</a></td>
                                    <td class="px-0 py-0"><a class="dropdown-item" href="/admin-js">Admin-JavaScript</a></td>
                                <th>
                            </tr>
                            <tr>
                                <th>
                                    <td class="px-0 py-0"><a class="dropdown-item" href="/admin-sql">Admin-SQL</a></td>
                                    <td class="px-0 py-0"><a class="dropdown-item" href="/admin-python">Admin-Python</a></td>
                                <th>
                            </tr>
                        </table>
                    </ul>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Dassboard
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <table class="table">
                            <tr>
                                <th>
                                    <td class="px-0 py-0"><a class="dropdown-item" href="/dassboard-php">PHP</a></td>
                                    <td class="px-0 py-0"><a class="dropdown-item" href="/dassboard-html">HTML</a></td>
                                <th>
                            </tr>
                            <tr>
                                <th>
                                    <td class="px-0 py-0"><a class="dropdown-item" href="/dassboard-css">CSS</a></td>
                                    <td class="px-0 py-0"><a class="dropdown-item" href="/dassboard-js">JavaScript</a></td>
                                <th>
                            </tr>
                            <tr>
                                <th>
                                    <td class="px-0 py-0"><a class="dropdown-item" href="/dassboard-sql">SQL</a></td>
                                    <td class="px-0 py-0"><a class="dropdown-item" href="/dassboard-python">Python</a></td>
                                <th>
                            </tr>
                        </table>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/text-editor">TextEditor</a>
                </li>
            </ul>
            <form class="d-flex">
                <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success" type="submit">Search</button>
            </form>
        </div>
    </div>
    </nav><br><br>

    <div class="container">
        <form action="/admin-phpStore" method="post">
            @csrf
            <div class="mb-3" style="width: 80%;">
                <label for="exampleFormControlTextarea1" class="form-label fs-5"><b>Judul</b></label>
                <textarea class="form-control border-dark" name="judul" id="exampleFormControlTextarea1" rows="3"></textarea>
            </div><br>
            <div class="mb-3" style="width: 80%;">
                <label for="formFileMultiple" class="form-label fs-5"><b>Image</b></label>
                <input class="form-control border-dark" name="image" type="file" id="formFileMultiple" multiple>
            </div><br>
            <div class="mb-3" style="width: 80%;">
                <label for="exampleFormControlTextarea1" class="form-label fs-5"><b>Isi</b></label>
                <textarea name="isi" id="myeditorinstance"></textarea>
            </div><br>
            <br>
            <div style="display: flex; justify-content: center;">
                <button type="submit" class="btn btn-sm btn-primary rounded-pill" style="width: 10%; height: 40px; "><i class="bi bi-file-earmark-text"></i><b> SIMPAN</b></button>
            </div>
        </form>
    </div>


     <script src="{{ asset('js/tinymce/tinymce.min.js') }}" referrerpolicy="origin"></script>
    <script>
    tinymce.init({
        selector: 'textarea#myeditorinstance', // Replace this CSS selector to match the placeholder element for TinyMCE
        plugins: 'code table lists',
        skin: "oxide-dark",
        toolbar: 'undo redo | formatselect| bold italic | alignleft aligncenter alignright | indent outdent | bullist numlist | code | table'
    });
    </script>


    
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
</body>
</html>