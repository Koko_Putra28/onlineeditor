<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dasboard</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="/css/style.css" rel="stylesheet">
</head>
<body>
    
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand me-5" href="#">OnlineEditor</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse ms-5" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Admin
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <table class="table">
                                <tr>
                                    <th>
                                        <td class="px-0 py-0"><a class="dropdown-item" href="/admin-php">Admin-PHP</a></td>
                                        <td class="px-0 py-0"><a class="dropdown-item" href="/admin-html">Admin-HTML</a></td>
                                    <th>
                                </tr>
                                <tr>
                                    <th>
                                        <td class="px-0 py-0"><a class="dropdown-item" href="/admin-css">Admin-CSS</a></td>
                                        <td class="px-0 py-0"><a class="dropdown-item" href="/admin-js">Admin-JavaScript</a></td>
                                    <th>
                                </tr>
                                <tr>
                                    <th>
                                        <td class="px-0 py-0"><a class="dropdown-item" href="/admin-sql">Admin-SQL</a></td>
                                        <td class="px-0 py-0"><a class="dropdown-item" href="/admin-python">Admin-Python</a></td>
                                    <th>
                                </tr>
                            </table>
                        </ul>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Dassboard
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <table class="table">
                                <tr>
                                    <th>
                                        <td class="px-0 py-0"><a class="dropdown-item" href="/dassboard-php">PHP</a></td>
                                        <td class="px-0 py-0"><a class="dropdown-item" href="/dassboard-html">HTML</a></td>
                                    <th>
                                </tr>
                                <tr>
                                    <th>
                                        <td class="px-0 py-0"><a class="dropdown-item" href="/dassboard-css">CSS</a></td>
                                        <td class="px-0 py-0"><a class="dropdown-item" href="/dassboard-js">JavaScript</a></td>
                                    <th>
                                </tr>
                                <tr>
                                    <th>
                                        <td class="px-0 py-0"><a class="dropdown-item" href="/dassboard-sql">SQL</a></td>
                                        <td class="px-0 py-0"><a class="dropdown-item" href="/dassboard-python">Python</a></td>
                                    <th>
                                </tr>
                            </table>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/text-editor">TextEditor</a>
                    </li>
                </ul>
                <form class="d-flex">
                    <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success" type="submit">Search</button>
                </form>
            </div>
        </div>
    </nav>
    <br><br>
    <div class="container">
        <div class="margin-bottom: 30px; margin-top:30px">  
            <img  src="{{ asset('images/programmer.jpg') }}" class="image-right" style="height: 15%; width: 30%;"/>
            <strong><h2 class="judul">What is Lorem Ipsum?</h2></strong>
            <p class="isi">
                <br>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                    It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. 
                    It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                    There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, 
                    or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, 
                    you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, 
                    making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. 
                    The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.
                <br>
            </p>
        </div>
        <br>
        <div style="margin-bottom: 30px; margin-top:30px">
            <img  src="{{ asset('images/php.jpg') }}" class="image-left" style="height: 15%; width: 30%;"/>
            <strong><h2 class="judul">Why do we use it?</h2></strong>
            <p class="isi">
            <br>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. 
                It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, 
                or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, 
                you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, 
                making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. 
                The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.
            <br>
            </p>
        </div>
       


        
    </div>

    
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
</body>
</html>