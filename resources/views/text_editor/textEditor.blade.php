<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dasboard</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
    
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">
        <a class="navbar-brand me-5" href="#">OnlineEditor</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse ms-5" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Admin
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <table class="table">
                            <tr>
                                <th>
                                    <td class="px-0 py-0"><a class="dropdown-item" href="/admin-php">Admin-PHP</a></td>
                                    <td class="px-0 py-0"><a class="dropdown-item" href="/admin-html">Admin-HTML</a></td>
                                <th>
                            </tr>
                            <tr>
                                <th>
                                    <td class="px-0 py-0"><a class="dropdown-item" href="/admin-css">Admin-CSS</a></td>
                                    <td class="px-0 py-0"><a class="dropdown-item" href="/admin-js">Admin-JavaScript</a></td>
                                <th>
                            </tr>
                            <tr>
                                <th>
                                    <td class="px-0 py-0"><a class="dropdown-item" href="/admin-sql">Admin-SQL</a></td>
                                    <td class="px-0 py-0"><a class="dropdown-item" href="/admin-python">Admin-Python</a></td>
                                <th>
                            </tr>
                        </table>
                    </ul>
                </li>
                
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Dassboard
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <table class="table">
                            <tr>
                                <th>
                                    <td class="px-0 py-0"><a class="dropdown-item" href="/dassboard-php">PHP</a></td>
                                    <td class="px-0 py-0"><a class="dropdown-item" href="/dassboard-html">HTML</a></td>
                                <th>
                            </tr>
                            <tr>
                                <th>
                                    <td class="px-0 py-0"><a class="dropdown-item" href="/dassboard-css">CSS</a></td>
                                    <td class="px-0 py-0"><a class="dropdown-item" href="/dassboard-js">JavaScript</a></td>
                                <th>
                            </tr>
                            <tr>
                                <th>
                                    <td class="px-0 py-0"><a class="dropdown-item" href="/dassboard-sql">SQL</a></td>
                                    <td class="px-0 py-0"><a class="dropdown-item" href="/dassboard-python">Python</a></td>
                                <th>
                            </tr>
                        </table>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/text-editor">TextEditor</a>
                </li>
            </ul>
            <form class="d-flex">
                <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success" type="submit">Search</button>
            </form>
        </div>
    </div>
    </nav>

    
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
</body>
</html>