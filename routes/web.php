<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DassController;
use App\Http\Controllers\EditorController;
use App\Http\Controllers\admin_php;
use App\Http\Controllers\admin_css;
use App\Http\Controllers\admin_html;
use App\Http\Controllers\admin_js;
use App\Http\Controllers\admin_sql;
use App\Http\Controllers\admin_python;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dassboard/dassboard-php');
});

//dassboard
Route::get('/dassboard-php', [DassController::class, 'dassboard_php']);
Route::get('/dassboard-css', [DassController::class, 'dassboard_css']);
Route::get('/dassboard-sql', [DassController::class, 'dassboard_sql']);
Route::get('/dassboard-html', [DassController::class, 'dassboard_html']);
Route::get('/dassboard-js', [DassController::class, 'dassboard_js']);
Route::get('/dassboard-python', [DassController::class, 'dassboard_python']);

//admin
    //admin php
    Route::get('/admin-php', [admin_php::class, 'index']);
    Route::get('/admin-phpCreate', [admin_php::class, 'create']);
    Route::post('/admin-phpStore', [admin_php::class, 'store']);
    Route::get('/admin-phpEdit/{id}', [admin_php::class, 'edit']);
    Route::post('/admin-phpUpdate/{id}', [admin_php::class, 'update']);
    Route::get('/admin-phpDelete/{id}', [admin_php::class, 'destroy']);

    //admin css
    Route::get('/admin-css', [admin_css::class, 'index']);

    //admin sql
    Route::get('/admin-sql', [admin_sql::class, 'index']);

    //admin html
    Route::get('/admin-html', [admin_html::class, 'index']);

    //admin javaScript
    Route::get('/admin-js', [admin_js::class, 'index']);

    //admin Python
    Route::get('/admin-python', [admin_python::class, 'index']);
//admin

//text-editor
Route::get('/text-editor', [EditorController::class, 'textEditor']);